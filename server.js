const express = require('express')

const app = express();

app.get('/', (req, res, next) => {
   res.send('Hello World!')
})

app.listen(3000, (error) => {
   if (error) return console.log('Hubo un error'), process.exit(1)   
   console.log('Platzigram listen on port 3000')
})

